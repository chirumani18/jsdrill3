function map(items, cb) {
    if (Array.isArray(items) == false && typeof cb != 'function') {
        return [];
    }
    let map_list = [];
    for (let index = 0; index < items.length; index++) {
        map_list.push(cb(items[index], index, items));
    }
    return map_list;
}
module.exports = map;