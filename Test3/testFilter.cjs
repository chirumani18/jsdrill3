const items = require('../items.cjs')
const result = require('../filter.cjs')

function filterNum(value, index, items) {
    if (typeof value == 'number') {
        if (value % 2 === 0) {
            return true;
        }
    }
    if (value.length > 4) {
        return true;
    }
}

console.log(result(items, filterNum));