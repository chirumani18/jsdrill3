function reduce(items, cb, startingValue) {
    // if (Array.isArray(items) == false && typeof cb != 'function') {
    //     return [];
    // }
    let accumulator;
    if (startingValue == undefined) {
        accumulator = items[0];
        for (let index = 1; index < items.length; index++) {
            accumulator = cb(accumulator, items[index], index, items);
        }
        return accumulator;
    }
    else {
        accumulator = startingValue;
        for (let index = 0; index < items.length; index++) {
            accumulator = cb(accumulator, items[index], index, items);
        }
        return accumulator;
    }

}
module.exports = reduce;
