function each(items, cb) {
    if (Array.isArray(items) == false) {
        return [];
    }
    for (let index = 0; index < items.length; index++) {
        items[index] = cb(items[index], index);
    }
    return items;
}

module.exports = each;