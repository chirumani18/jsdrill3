
function find(items, cb, number) {
    if (Array.isArray(items) == false) {
        return [];
    }
    const new_list = [];
    for (let value of items) {
        if (cb(value, number) != undefined) {
            new_list.push(cb(value, number));
        }
    }
    if (new_list.length > 0) {
        return new_list;
    }
    return 'undefined';
}

module.exports = find;