function filter(items, cb) {
    if (Array.isArray(items) == false && typeof cb != 'function') {
        return [];
    }
    let filter_list = [];

    for (let index = 0; index < items.length; index++) {
        if (cb(items[index], index, items) == true) {
            filter_list.push(items[index]);
        }
    }
    return filter_list;
}

module.exports = filter;