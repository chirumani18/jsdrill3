function flatten(items1, depth = 1) {
    let flat_array = [];
    let currentDepth = 0;
    for (let index = 0; index < items1.length; index++) {
        if (Array.isArray(items1[index]) && currentDepth < depth) {
            flat_array = flat_array.concat(flatten(items1[index], depth - 1));
        }
        else {
            if (items1[index] != undefined) {
                flat_array.push(items1[index]);
            }
        }
    }

    return flat_array;
}
module.exports = flatten;